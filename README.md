# Malá Práce

## Popis projektu

Tento projekt slouží k automatizovanému generování statických HTML stránek z Markdown souborů pomocí MKDocs a GitLab CI/CD. Součástí je také kontrola kvality Markdown souborů pomocí nástroje markdownlint.

## Použití

### Instalace

Nejprve je třeba zajistit splnění prerekvizit pro tento projekt:

1. **Git**: Pokud jej nemáte nainstalován, můžete si jej stáhnout a nainstalovat z oficiálních stránek [Git](https://git-scm.com/downloads).
2. **GitLab účet**: Založte si účet na platformě GitLab, pokud jej ještě nemáte. Projekt bude hostován zde.

### Nastavení

Po splnění prerekvizit je třeba připravit repozitář na GitLabu. Postupujte podle následujících kroků:

1. **Vytvoření projektu na GitLabu**: Přihlaste se do svého účtu na GitLabu a vytvořte nový projekt.
2. **Klonování repozitáře**: Naklonujte si repozitář do svého počítače pomocí příkazu:

    ```bash
    git clone https://gitlab.com/MartySagin1/malapracekal0326.git
    cd malapracekal0326
    ```

### Spuštění

Po úspěšném klonování repozitáře můžete začít pracovat s projektem.

## Jak používat tento projekt?

Po naklonování repozitáře do vašeho prostředí můžete začít pracovat s Markdown soubory ve složce projektu.

1. **Úprava Markdown souborů**: Upravujte existující nebo vytvářejte nové Markdown soubory pro obsah vašich stránek.
2. **Testování kvality Markdown souborů**: Spusťte test kvality Markdown souborů pomocí příkazu:

    ```bash
    markdownlint *.md
    ```

3. **Generování HTML stránek**: Proveďte generování statických HTML stránek z Markdown souborů pomocí MKDocs:

    ```bash
    mkdocs build
    ```

4. **Nasazení**: Automatické nasazení je nakonfigurováno v GitLab CI/CD pipeline, který vygenerované HTML stránky automaticky nasadí na GitLab Pages.

## Contributing

Přispění k projektu je vítáno! Pro příspěvky postupujte následovně:

1. Vytvořte novou větev s názvem odpovídajícím vaší funkci (`git checkout -b feature/NovaFunkce`).
2. Proveďte své změny.
3. Commitněte své změny (`git commit -am 'Přidána nová funkce: NovaFunkce'`).
4. Pushněte svou větev do repozitáře (`git push origin feature/NovaFunkce`).
5. Vytvořte nový pull request.

## Licence

[MIT Licence](LICENSE)
